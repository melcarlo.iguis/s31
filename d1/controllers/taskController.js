const Task = require('../models/taskSchema')

// Get all task
module.exports.getAllTasks = () =>{
	return Task.find({}).then(result =>{
		return result
	})
}

// create new task
module.exports.createTask = (reqBody) => {

	let newTask = new Task({

		name : reqBody.name
	})

	return newTask.save().then((task, err) => {
		if(err){
			console.log(err)
			return false
		}
		else{
			return task
		}
	})
}


// Delete task
module.exports.deleteTask = (taskId) => {

	return Task.findByIdAndDelete(taskId).then((removedTask, err)=>{
	if(err){
		console.log(err)
		return false
	}else{
		return removedTask
	}
	})
}

// update task controller

module.exports.updateTask = (taskId, newContent) => {

	return Task.findById(taskId).then((result, err) => {

		if(err){
			console.log(err)
			return false
		}

		result.name = newContent.name
			return result.save().then((updatedTask, saveErr) => {

				if(saveErr){
					console.log(saveErr)
					return false
				} else {

					return updatedTask
				}
			})
	})
}




// ---------------------------------------------------------------------------------

// ACTIVITY CODE


// create a controller function for retrieving a specific task

module.exports.retrieveTask = (taskId) => {

	return Task.findById(taskId).then((findTask, err) =>{

		if(err){
			console.log(err)
			return false
		}else{
			return findTask
		}
	})
}

// create a controller function for changing the status of a task to complete

module.exports.updateTask = (taskId, newContent) => {

	return Task.findById(taskId).then((result, err) => {

		if(err){
			console.log(err)
			return false
		}

		result.status = newContent.status
			return result.save().then((updatedTask, saveErr) => {

				if(saveErr){
					console.log(saveErr)
					return false
				} else {

					return updatedTask
				}
			})
	})
}
