// Contain all tasks endpoint for our applications
const express = require('express')
const router = express.Router()
const taskController = require('../controllers/taskController')


router.get('/' , (req, res) => {

	taskController.getAllTasks().then(resultFromController => 
		res.send(resultFromController))
})

// create new
router.post('/createTask' , (req, res) => {

	taskController.createTask(req.body).then(resultFromController => 
		res.send(resultFromController))
})

// delete
router.delete('/deleteTask/:id' , (req, res) => {

	taskController.deleteTask(req.params.id).then(resultFromController =>
		res.send(resultFromController))
})

// update

router.put('/updateTask/:id', (req, res) => {

	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
})

// ---------------------------------------------------------------------------------
// ACTIVITY CODE

// create a route for getting a specific task
router.get('/:id', (req, res) =>{

	taskController.retrieveTask(req.params.id).then(resultFromController=> res.send(resultFromController))
})
module.exports = router


// create a get route for changing the status of a task to "complete"

router.put('/:id/complete', (req, res) => {

	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
})